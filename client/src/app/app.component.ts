import {Component, OnInit} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {Person} from '../models/person';
import gql from 'graphql-tag';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  persons: Person[];
  person: Person;

  personCreated = false;
  personDeleted = false;
  personUpdated = false;
  personNotFound = false;

  constructor(private apollo: Apollo) {
  }

  ngOnInit() {
  }

  getPersons() {
    this.reset();
    const getPersons = gql`
      query {
        getPersons {
           id,
           firstName,
           lastName,
        }}`;

    this.apollo.query({
      query: getPersons,
      fetchPolicy: "network-only"
    }).subscribe(
      (res: any) => {
        this.persons = res.data.getPersons;
        return this.persons;
      },
      () => {
      }
    );
  }

  findPerson(firstName, lastName) {
    this.reset();
    const findPerson = gql`
      query find($firstName: String!, $lastName: String!) {
        findPerson(input: {
           firstName: $firstName,
           lastName: $lastName
        }) {
           id,
           firstName,
           lastName,
        }}`;

    this.apollo.query({
      query: findPerson,
      variables: {
        firstName: firstName,
        lastName: lastName
      },
    }).subscribe(
      (res: any) => {
        if (res.data.findPerson === null) {
          return this.personNotFound = true;
        }
        return this.person = res.data.findPerson;
      },
      () => {
        return this.personNotFound = true;
      }
    );
  }

  createPerson(firstName, lastName) {
    this.reset();
    const createPerson = gql`
      mutation create($firstName: String!, $lastName: String!) {
        createPerson(input: {
           firstName: $firstName,
           lastName: $lastName
        }) {
           id,
           firstName,
           lastName,
        }}`;

    this.apollo.mutate({
      mutation: createPerson,
      variables: {
        firstName: firstName,
        lastName: lastName
      },
    }).subscribe(() => {
      this.personCreated = true;
    });
  }

  deletePerson(firstName, lastName) {
    this.reset();
    const deletePerson = gql`
    mutation delete($firstName: String!, $lastName: String!) {
       deletePerson(input: {
           firstName: $firstName,
           lastName: $lastName
        }) {
           id,
           firstName,
           lastName,
        }}`;

    this.apollo.mutate({
      mutation: deletePerson,
      variables: {
        firstName: firstName,
        lastName: lastName
      },
    }).subscribe(() => {
      this.personDeleted = true;
    });
  }

  updatePerson(id,firstName, lastName) {
    this.reset();
    const updatePerson = gql`
    mutation update($id: ID!, $person: PersonInput!) {
      updatePerson(input: {
      id: $id,
      person: $person,
     }) {
        id,
        firstName,
        lastName,
      }}`;

    this.apollo.mutate({
      mutation: updatePerson,
      variables: {
        id: id,
        person: {
          firstName: firstName,
          lastName: lastName,
        }
      },
    }).subscribe(res => {
      this.personUpdated = true;
    });
  }

  reset() {
    this.person = undefined;
    this.personNotFound = false;
  }
}
