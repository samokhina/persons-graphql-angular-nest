import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Person } from 'person/person.entity';

@Injectable()
export class PersonService {
  constructor(
    @InjectRepository(Person)
    private readonly personRepository: Repository<Person>,
  ) {}

  async getPerson(lastName): Promise<Person> {
    return await this.personRepository.findOne({lastName: lastName});
  }

  async createPerson(person: Person): Promise<Person> {
    console.log('PersonService#create', person);
    return await this.personRepository.save(person);
  }

  async updateLastName(oldName: string, newName: string): Promise<any> {
    let personToUpdate = await this.personRepository.findOne({lastName: oldName});
    personToUpdate.lastName = newName;
    return await this.personRepository.save(personToUpdate);
  }

  async deletePerson(lastName: string): Promise<any> {
    let personToDelete = await this.personRepository.findOne({lastName: lastName});
    return await this.personRepository.delete(personToDelete);
  }
}
